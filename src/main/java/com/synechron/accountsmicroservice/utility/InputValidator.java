package com.synechron.accountsmicroservice.utility;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.util.StringUtils;

import com.synechron.accountsmicroservice.exception.EmptyInputException;
import com.synechron.accountsmicroservice.model.Account;

public class InputValidator {
	
	public static void checkForEmptyInput(Account account) {
		if (account.getAccountBalance() == null || (account.getAccountBalance().compareTo(BigDecimal.ZERO) < 0))
			throw new EmptyInputException("Please enter valid amount!");
		if (!StringUtils.hasText(account.getCustomer().getName()))
			throw new EmptyInputException("Name field can not be blank!");
		if (!StringUtils.hasText(account.getCustomer().getAddress()))
			throw new EmptyInputException("Address field can not be blank!");
		if (!StringUtils.hasText(account.getCustomer().getEmail()))
			throw new EmptyInputException("Email field can not be blank!");
		if (!StringUtils.hasText(account.getCustomer().getPhoneNumber())
				|| (StringUtils.hasText(account.getCustomer().getPhoneNumber())
						&& account.getCustomer().getPhoneNumber().length() != 10))
			throw new EmptyInputException("Please enter valid 10 digit phone no without Country Code!");

	}
	
	public static LocalDate checkDateFormatAndIfEmptyInitialize(DateTimeFormatter formatter, String date) {
		try {
			if (StringUtils.hasText(date)) {
				return LocalDate.parse(date, formatter);
			} else {
				return LocalDate.now();
			}
		} catch (DateTimeParseException e) {
			throw new IllegalArgumentException("Enter date in proper format : yyyy-MM-dd");
		}
	}

}
