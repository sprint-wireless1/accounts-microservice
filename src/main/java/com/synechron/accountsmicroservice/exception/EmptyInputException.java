package com.synechron.accountsmicroservice.exception;

public class EmptyInputException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmptyInputException(String errorMsg) {
		super(errorMsg);
	}

}
