package com.synechron.accountsmicroservice.model;

public enum TransactionType {
	CREDIT, DEBIT;
}
