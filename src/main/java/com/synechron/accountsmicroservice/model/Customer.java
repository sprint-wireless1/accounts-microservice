package com.synechron.accountsmicroservice.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "account")
@ToString(exclude = "account")
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long customerId;
	
	@JsonProperty(value = "name", required = true)
	private String name;

	@JsonProperty(value = "email", required = true)
	private String email;

	@JsonProperty(value = "phoneNumber", required = true)
	private String phoneNumber;
	
	@JsonProperty(value = "address", required = true)
	private String address;
	
//	@JsonIgnore
//	@JsonProperty(value = "password", required = true)
//	private String password;
//	@JsonProperty(value = "creditScore", required = true)
//	private int creditScore;
	
    @JsonIgnore
	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    private Account account;
	
}
