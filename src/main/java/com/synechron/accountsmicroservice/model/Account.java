package com.synechron.accountsmicroservice.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "account")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "account_id")
    private Long accountId;
	
	@JsonProperty(value = "customer", required = true)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name= "customer_id")
	private Customer customer;
	
	@JsonProperty(value = "accountBalance", required = true)
	private BigDecimal accountBalance;
	
}
