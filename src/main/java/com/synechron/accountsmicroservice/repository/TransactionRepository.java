package com.synechron.accountsmicroservice.repository;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.accountsmicroservice.model.Account;
import com.synechron.accountsmicroservice.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{
	
	Page<Transaction> findByAccountAndTransactionTimestampBetween(Account accountId, LocalDateTime startDate,
			LocalDateTime endDate, Pageable pageRequest);
	
	Page<Transaction> findByAccount(Account account, Pageable pageRequest);

}