package com.synechron.accountsmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.accountsmicroservice.model.Account;

@Repository
public interface AccountsRepository extends JpaRepository<Account, Long>{

	Account getAccountBalanceByAccountId(Long accountId);

}
