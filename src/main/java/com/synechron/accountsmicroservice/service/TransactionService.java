package com.synechron.accountsmicroservice.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.synechron.accountsmicroservice.model.Account;
import com.synechron.accountsmicroservice.model.Transaction;
import com.synechron.accountsmicroservice.model.TransactionType;
import com.synechron.accountsmicroservice.repository.TransactionRepository;
import com.synechron.accountsmicroservice.utility.InputValidator;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TransactionService {
	
	private final AccountsService accountsService;
	private final TransactionRepository transactionRepository;
	
	public Transaction doTransaction(Transaction transaction, Long accountId) {

		if (accountId != null) {
			Account account = accountsService.getAccountDetails(accountId);
			
			if ((transaction.getTransactionAmount().compareTo(BigDecimal.ZERO) > 0)) {
				
				if (TransactionType.CREDIT.equals(transaction.getTransactionType()))
					account.setAccountBalance(account.getAccountBalance().add(transaction.getTransactionAmount()));
				else if (TransactionType.DEBIT.equals(transaction.getTransactionType())
						&& (account.getAccountBalance().compareTo(transaction.getTransactionAmount()) >= 0))
					account.setAccountBalance(account.getAccountBalance().subtract(transaction.getTransactionAmount()));
				else
					throw new IllegalArgumentException("Account Balance is low to perform debit!");
				
			} else {
				throw new IllegalArgumentException("Enter amount greater than 0 to credit/debit!");
			}
			
			transaction.setAccount(account);
			return this.transactionRepository.save(transaction);
			
		} else {
			throw new IllegalArgumentException("Invalid account id passed");
		}

	}

	public Map<String, Object> getStatement(Long accountId, int page, int size) {
		
		Account account = accountsService.getAccountDetails(accountId);
		
		 Pageable pageRequest = PageRequest.of(page, size);
	        final Page<Transaction> pageResponse = this.transactionRepository.findByAccount(account, pageRequest);
	        final long totalRecords = pageResponse.getTotalElements();
	        final int pages = pageResponse.getTotalPages();
	        final List<Transaction> data = pageResponse.getContent();

	        //Create a linkedHashMap
	        Map<String, Object> resultMap = new LinkedHashMap<>();
	        resultMap.put("transactions", data);
	        resultMap.put("total-records", totalRecords);
	        resultMap.put("pages", pages);
	        return resultMap;
	}

	public List<Transaction> getSortedStatementByDate(Long accountId, String startDate, String endDate, String sort,
			String sort_order, int page, int size) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
		LocalDate finalStartDate = InputValidator.checkDateFormatAndIfEmptyInitialize(formatter, startDate);
		LocalDate finalEndDate = InputValidator.checkDateFormatAndIfEmptyInitialize(formatter, endDate);
		
		Account account = accountsService.getAccountDetails(accountId);

		Pageable pageRequest = null;

		if (sort_order.equalsIgnoreCase("desc"))
			pageRequest = PageRequest.of(page, size, Direction.DESC, sort);
		else if (sort_order.equalsIgnoreCase("asc"))
			pageRequest = PageRequest.of(page, size, Direction.ASC, sort);
		
		final Page<Transaction> pageResponse = this.transactionRepository.findByAccountAndTransactionTimestampBetween(
				account, finalStartDate.atStartOfDay(), finalEndDate.atTime(23, 59, 59), pageRequest);
		return pageResponse.getContent();
	}

}
