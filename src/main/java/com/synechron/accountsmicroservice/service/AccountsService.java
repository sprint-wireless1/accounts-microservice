package com.synechron.accountsmicroservice.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.synechron.accountsmicroservice.model.Account;
import com.synechron.accountsmicroservice.repository.AccountsRepository;
import com.synechron.accountsmicroservice.utility.InputValidator;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AccountsService {

	private final AccountsRepository accountsRepository;

	public Account createAccount(Account account) {
		InputValidator.checkForEmptyInput(account);
		Account savedAccount = this.accountsRepository.save(account);
		return savedAccount;
	}

	public Account getAccountDetails(Long accountId) {
		return this.accountsRepository.findById(accountId).orElseThrow(() -> new IllegalArgumentException("Invalid account id passed, no such account exist!"));
	}

	public BigDecimal getAccountBalance(Long accountId) {
		BigDecimal accBal = null;
		try {
			accBal = this.accountsRepository.getAccountBalanceByAccountId(accountId).getAccountBalance();
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid account id passed, no such account exist!");
		}
		return accBal;
	}	

}
