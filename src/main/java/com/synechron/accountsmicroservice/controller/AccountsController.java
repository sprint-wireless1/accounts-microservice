package com.synechron.accountsmicroservice.controller;

import static org.springframework.http.HttpStatus.CREATED;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.accountsmicroservice.model.Account;
import com.synechron.accountsmicroservice.model.Transaction;
import com.synechron.accountsmicroservice.service.AccountsService;
import com.synechron.accountsmicroservice.service.TransactionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/account")
@RequiredArgsConstructor
@CrossOrigin
public class AccountsController {
	
	private final AccountsService accountsService;
	private final TransactionService transactionService;
	
	@PostMapping("/openAccount")
	@ResponseStatus(CREATED)
	public Account createAccount(@RequestBody Account account) {
		return this.accountsService.createAccount(account);
	}

	@GetMapping("/{id}")
	public Account getAccountDetails(@PathVariable("id") Long accountId) {
		
		return this.accountsService.getAccountDetails(accountId);
	}
	
	@GetMapping("/balance/{id}")
	public BigDecimal getAccountBalance(@PathVariable("id") Long accountId) {
		
		return this.accountsService.getAccountBalance(accountId);
	}
	
	@PostMapping("/transaction/{id}")
	public Transaction doTransaction(@PathVariable("id") Long accountId, @RequestBody Transaction transaction) {
		
		return this.transactionService.doTransaction(transaction, accountId);
	}
	
	@GetMapping("/{id}/statement")
	public Map<String, Object> getStatement(@PathVariable("id") Long accountId,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size) {
		
		return this.transactionService.getStatement(accountId, page, size);		
	}
	
	@GetMapping("/{id}/statementByDate")
	public List<Transaction> getSortedStatementByDate(@PathVariable("id") Long accountId, 
			@RequestParam(name = "start_date", required = false) String startDate, 
			@RequestParam(name = "end_date", required = false) String endDate,
			@RequestParam(name = "sort", required = false, defaultValue = "transactionAmount") String sort,
			@RequestParam(name = "sort_order", required = false, defaultValue = "asc") String sort_order,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size) {
		
		return this.transactionService.getSortedStatementByDate(accountId, startDate, endDate, sort, sort_order, page, size);
	}

}